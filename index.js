const http = require("http");
const port = 4000;

// Mock database

http.createServer((request, response) =>{
	//GET Method
	if(request.url == "/" && request.method == "GET"){

		response.writeHead(200, {"Content-Type":"text/plain"});
		//JSON.stringify() method it will convert the desired output data to string 
		// End the response process
		response.end("Welcome to booking system");
		}

		else if(request.url == "/profile" && request.method == "GET"){

		response.writeHead(200, {"Content-Type":"text/plain"});
		//JSON.stringify() method it will convert the desired output data to string 
		// End the response process
		response.end("Welcome to your profile");
		}
		else if(request.url == "/courses" && request.method == "GET"){

		response.writeHead(200, {"Content-Type":"text/plain"});
		//JSON.stringify() method it will convert the desired output data to string 
		// End the response process
		response.end("Here's our courses available.");
		}
		else if(request.url == "/addcourse" && request.method == "POST"){

		response.writeHead(200, {"Content-Type":"text/plain"});
		//JSON.stringify() method it will convert the desired output data to string 
		// End the response process
		response.end("Add course to our resources.");
		}
		else if(request.url == "/updatecourse" && request.method == "PUT"){

		response.writeHead(200, {"Content-Type":"text/plain"});
		//JSON.stringify() method it will convert the desired output data to string 
		// End the response process
		response.end("Update course to our resources.");
		}
		else if(request.url == "/archivecourse" && request.method == "DELETE"){

		response.writeHead(200, {"Content-Type":"text/plain"});
		//JSON.stringify() method it will convert the desired output data to string 
		// End the response process
		response.end("Archive course to our resources.");
		}
		else{
			response.writeHead(404, {"Content-Type": "text/plain"})
			response.end("Page is not available");
		}



	}).listen(port);

console.log(`Server is running at localhost:${port}`);
